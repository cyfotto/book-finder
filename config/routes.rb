Rails.application.routes.draw do
  get 'search', to: 'books#search'
  get 'get_author', to: 'books#get_author'
  get 'book_info', to: 'books#book_info'
  post 'search', to: 'books#get_books'
  root to: 'books#search'
end
