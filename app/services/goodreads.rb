module Goodreads
  include HTTParty
  include OAuth
  base_uri "https://www.goodreads.com"

  class << self
    attr_accessor :options
  end

  self.options = {}

  def self.new(params = {})
    Goodreads::Client.new(params)
  end

  def self.configure(params = {})
    raise(ArgumentError, "Options hash required!") unless params.is_a?(Hash)

    options[:api_key] = params[:api_key]
    options[:api_secret] = params[:api_secret]
    options
  end

  def self.configuration
    options
  end

  def self.reset_configuration
    self.options = {}
  end

  def self.oauth_request(http_method, path, params = {})
    fail "OAuth access token required!" unless @oauth_token

    headers = { "Accept" => "application/xml" }

    if http_method == :get || http_method == :delete
      if params
        url_params = params.map { |k, v| "#{k}=#{v}" }.join("&")
        path = "#{path}?#{url_params}"
      end
      @oauth_token.request(http_method, path, headers)
    end
    @oauth_token.request(http_method, path, params || {}, headers)
  end

  def self.parse(req)
      hash = Hash.from_xml(req.body)["GoodreadsResponse"]
      hash.delete("Request")
      hash
  end

  # TODO: Customs errors.
  class Error < StandardError; end

  # TODO: Extract modules into seperate files.
  module Reviews
    def review(id)
      data = Goodreads.get("/review/show", query: id)
      hash = Goodreads.parse(data)
      Hashie::Mash.new(hash["review"])
    end

    def reviews(params = {})
      data = Goodreads.get("/review/list", query: params.merge(v: "2"))
      hash = Goodreads.parse(data)
      reviews = hash["reviews"]["review"]
      if reviews.present?
        reviews.map { |review| Hashie::Mash.new(review) }
      else
        []
      end
    end

    def user_reviews(user_id, book_id, params = {})
      data = Goodreads.get("review/show_by_user_and_book.xml", query: params.merge(user_id: user_id, book_id: book_id))
      hash = Goodreads.parse(data)
      Hashie::Mash.new(hash["review"])
    end

    def create_review(book_id, params = {})
      params = params.merge(book_id: book_id)

      params[:read_at] = params[:read_at].strftime("%Y-%m-%d") if params[:read_at].is_a(Time)

      params[:'review[rating]'] = params.delete(:rating) if params[:rating]
      params[:'review[read_at]'] = params.delete(:read_at) if params[:read_at]
      params[:'review[review]'] = params.delete(:review) if params[:review]

      data = Goodreads.oauth_request(:post, "/review.xml", params)
      hash = Goodreads.parse(data)
      Hashie::Mash.new(hash["review"])
    end

    def edit_review(review_id, params = {})
      params[:read_at] = params[:read_at].strftime("%Y-%m-%d") if params[:read_at].is_a(Time)

      params[:'review[rating]'] = params.delete(:rating) if params[:rating]
      params[:'review[read_at]'] = params.delete(:read_at) if params[:read_at]
      params[:'review[review]'] = params.delete(:review) if params[:review]

      data = Goodreads.oauth_request(:put, "/review/#{review_id}.xml", params)
      hash = Goodreads.parse(data)
      Hashie::Mash.new(hash["review"])
    end
  end

  module Books
    def books(query, params = {})
      params[:q] = query.to_s.strip
      return nil if params[:q].blank?
      params[:page] = 1 if !params[:page] || params[:page].to_i < 1

      results = []
      data = Goodreads.get("/search/index.xml", query: params, timeout: 10)
      hash = Goodreads.parse(data)
      hash = Hashie::Mash.new(hash["search"])
      results.push(hash)
      sleep 1

      results
    end

    def book(book_id, params = {})
      data = Goodreads.get("https://www.goodreads.com/book/show/#{book_id}.xml", query: params, timeout: 10)
      hash = Goodreads.parse(data)
      Hashie::Mash.new(hash)
    end
  end

  module Authors
    def author(id, params = {})
      params[:id] = id
      data = Goodreads.get("/author/show", query: params)
      hash = Goodreads.parse(data)
      Hashie::Mash.new(hash)
    end

    # Search author by name.
    def find_author(name, key)
      name_encoded = ERB::Util.url_encode(name)
      res = Goodreads.get("/api/author_url/#{name_encoded}", query: key, timeout: 60)
      hash = Goodreads.parse(res)
      Hashie::Mash.new(hash)
    end

    def author_books(name, key)
      author_info = find_author(name, key)
      author_id = author_info.author.id
      res = Goodreads.get("/author/list/#{author_id}", query: params.merge(key: key), timeout: 60)
      hash = Goodreads.parse(res)
      Hashie::Mash.new(hash)
      #TODO: Add the ability to search by pages.
    end
  end

  class Client
    include Goodreads::Books
    include Goodreads::Authors
    include Goodreads::Reviews

    attr_reader :api_key, :api_secret, :oauth_token

    def initialize(options = {})
      raise(ArgumentError, "Options hash required.") unless options.is_a?(Hash)

      @api_key = options[:api_key] || Goodreads.configuration[:api_key]
      @api_secret = options[:api_secret] || Goodreads.configuration[:api_secret]
      @oauth_token = options[:oauth_token]
    end
  end
end
