class BooksController < ApplicationController
  def search
  end

  def get_books
    client = Goodreads::Client.new(api_key: ENV['KEY'])
    param = params[:query] || search_params
    @results, @query_param = client.book_search(param, page: params[:page], key: client.api_key), param
  end

  def get_author
    client = Goodreads::Client.new(api_key: ENV['KEY'])
    author_name = params[:query].to_s.strip
    redirect_to client.find_author(author_name, key: client.api_key).author.link
  end

  def book_info
    client = Goodreads::Client.new(api_key: ENV['KEY'])
    book_id = params[:query].to_s.strip
    @results = client.get_book_info(book_id, key: client.api_key)
  end

  def author_info
  end

  private

  def search_params
    params.require(:books).permit(:query)[:query]
  end
end
